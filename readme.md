# @apiclient.xyz/docker

easy communication with docker remote api from node, TypeScript ready

## Install

To install @apiclient.xyz/docker, you can use npm (npm package manager). Run the following command in your terminal:

```bash
npm install @apiclient.xyz/docker --save
```

This command installs the package and adds it as a dependency to your project's `package.json` file.

## Usage

The `@apiclient.xyz/docker` package provides a TypeScript-ready interface for interacting with Docker's Remote API directly from Node.js applications. It leverages TypeScript for strong type definitions, ensuring more reliable and maintainable code.

### Prerequisites

Before you begin, ensure:

- You have Docker installed and running on your machine or a remote server.
- You are familiar with TypeScript and have it set up in your development environment.

### Getting Started

First, import the required classes from the package:

```typescript
import { DockerHost, DockerContainer, DockerService, DockerNetwork } from '@apiclient.xyz/docker';
```

### Instantiate DockerHost

Start by creating a `DockerHost` instance. This class is the entry point to communicate with the Docker Remote API.

```typescript
// Connect to local Docker instance
const localDockerHost = new DockerHost();

// Or specify a custom path or URL to a Docker host
const remoteDockerHost = new DockerHost('tcp://<REMOTE_DOCKER_HOST>:2375');
```

### Working with Containers

#### List All Containers

```typescript
async function listAllContainers() {
  const containers = await localDockerHost.getContainers();
  console.log(containers);
}

listAllContainers();
```

#### Create and Remove a Container

```typescript
import { IContainerCreationDescriptor } from '@apiclient.xyz/docker';

async function createAndRemoveContainer() {
  const containerDescriptor: IContainerCreationDescriptor = {
    Hostname: 'test-container',
    Domainname: '',
    // Additional settings here
  };

  // Create container
  const container = await DockerContainer.create(localDockerHost, containerDescriptor);
  console.log(`Container Created: ${container.Id}`);

  // Remove container
  await container.remove();
  console.log(`Container Removed: ${container.Id}`);
}

createAndRemoveContainer();
```

### Working with Docker Services

#### Create a Docker Service

```typescript
import { IServiceCreationDescriptor } from '@apiclient.xyz/docker';

async function createDockerService() {
  const serviceDescriptor: IServiceCreationDescriptor = {
    name: 'my-service',
    image: 'nginx:latest', // Docker Image
    // Additional settings
  };
  
  const service = await DockerService.createService(localDockerHost, serviceDescriptor);
  console.log(`Service Created: ${service.Id}`);
}

createDockerService();
```

### Working with Docker Networks

#### Listing and Creating Networks

```typescript
async function listAndCreateNetwork() {
  // List all networks
  const networks = await localDockerHost.getNetworks();
  console.log(networks);
  
  // Create a new network
  const network = await DockerNetwork.createNetwork(localDockerHost, {
    Name: 'my-network'
    // Additional settings
  });
  console.log(`Network Created: ${network.Id}`);
}

listAndCreateNetwork();
```

### Advanced Usage

You can leverage the full potential of the Docker Remote API with `@apiclient.xyz/docker`. This includes managing images, volumes, swarms, and more. The package's design is consistent and intuitive, making it easy to extend your usage as needed.

Remember, the Docker Remote API offers extensive capabilities. Always refer to the [Docker API documentation](https://docs.docker.com/engine/api/latest/) for a comprehensive list of endpoints and actions you can perform.

### Conclusion

`@apiclient.xyz/docker` simplifies interaction with Docker's Remote API in TypeScript projects, providing strong typing and asynchronous operations. Whether you're managing containers, images, services or networks, it offers a comprehensive toolset to perform these tasks seamlessly.

## License and Legal Information

This repository contains open-source code that is licensed under the MIT License. A copy of the MIT License can be found in the [license](license) file within this repository. 

**Please note:** The MIT License does not grant permission to use the trade names, trademarks, service marks, or product names of the project, except as required for reasonable and customary use in describing the origin of the work and reproducing the content of the NOTICE file.

### Trademarks

This project is owned and maintained by Task Venture Capital GmbH. The names and logos associated with Task Venture Capital GmbH and any related products or services are trademarks of Task Venture Capital GmbH and are not included within the scope of the MIT license granted herein. Use of these trademarks must comply with Task Venture Capital GmbH's Trademark Guidelines, and any usage must be approved in writing by Task Venture Capital GmbH.

### Company Information

Task Venture Capital GmbH  
Registered at District court Bremen HRB 35230 HB, Germany

For any legal inquiries or if you require further information, please contact us via email at hello@task.vc.

By using this repository, you acknowledge that you have read this section, agree to comply with its terms, and understand that the licensing of the code does not imply endorsement by Task Venture Capital GmbH of any derivative works.
