# Changelog

## 2024-10-13 - 1.2.5 - fix(dependencies)
Update dependencies for stability improvements

- Updated @push.rocks/smartstream to version ^3.0.46
- Updated @push.rocks/tapbundle to version ^5.3.0
- Updated @types/node to version 22.7.5

## 2024-10-13 - 1.2.4 - fix(core)
Refactored DockerImageStore constructor to remove DockerHost dependency

- Adjusted DockerImageStore constructor to remove dependency on DockerHost
- Updated ts/classes.host.ts to align with DockerImageStore's new constructor signature

## 2024-08-21 - 1.2.3 - fix(dependencies)
Update dependencies to the latest versions and fix image export test

- Updated several dependencies to their latest versions in package.json.
- Enabled the previously skipped 'should export images' test.

## 2024-06-10 - 1.2.1-1.2.2 - Core/General
General updates and fixes.

- Fix core update

## 2024-06-10 - 1.2.0 - Core
Core updates and bug fixes.

- Fix core update

## 2024-06-08 - 1.2.0 - General/Core
Major release with core enhancements.

- Processing images with extraction, retagging, repackaging, and long-term storage

## 2024-06-06 - 1.1.4 - General/Imagestore
Significant feature addition.

- Add feature to process images with extraction, retagging, repackaging, and long-term storage

## 2024-05-08 - 1.0.112 - Images
Add new functionality for image handling.

- Can now import and export images
- Start work on local 100% JS OCI image registry

## 2024-06-05 - 1.1.0-1.1.3 - Core
Regular updates and fixes.

- Fix core update

## 2024-02-02 - 1.0.105-1.0.110 - Core
Routine core updates and fixes.

- Fix core update

## 2022-10-17 - 1.0.103-1.0.104 - Core
Routine core updates.

- Fix core update

## 2020-10-01 - 1.0.99-1.0.102 - Core
Routine core updates.

- Fix core update

## 2019-09-22 - 1.0.73-1.0.78 - Core
Routine updates and core fixes.

- Fix core update

## 2019-09-13 - 1.0.60-1.0.72 - Core
Routine updates and core fixes.

- Fix core update

## 2019-08-16 - 1.0.43-1.0.59 - Core
Routine updates and core fixes.

- Fix core update

## 2019-08-15 - 1.0.37-1.0.42 - Core
Routine updates and core fixes.

- Fix core update

## 2019-08-14 - 1.0.31-1.0.36 - Core
Routine updates and core fixes.

- Fix core update

## 2019-01-10 - 1.0.27-1.0.30 - Core
Routine updates and core fixes.

- Fix core update

## 2018-07-16 - 1.0.23-1.0.24 - Core
Routine updates and core fixes.

- Fix core shift to new style

## 2017-07-16 - 1.0.20-1.0.22 - General
Routine updates and fixes.

- Update node_modules within npmdocker

## 2017-04-02 - 1.0.18-1.0.19 - General
Routine updates and fixes.

- Work with npmdocker and npmts 7.x.x
- CI updates

## 2016-07-31 - 1.0.17 - General
Enhancements and fixes.

- Now waiting for response to be stored before ending streaming request
- Cosmetic fix

## 2016-07-29 - 1.0.14-1.0.16 - General
Multiple updates and features added.

- Fix request for change observable and add npmdocker
- Add request typings

## 2016-07-28 - 1.0.13 - Core
Fixes and preparations.

- Fixed request for newer docker
- Prepare for npmdocker
 

## 2016-06-16 - 1.0.0-1.0.2 - General
Initial sequence of releases, significant feature additions and CI setups.

- Implement container start and stop
- Implement list containers and related functions
- Add tests with in docker environment 

## 2016-04-12 - unknown - Initial Commit
Initial project setup.

- Initial commit

