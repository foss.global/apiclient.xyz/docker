/**
 * creates a new Network
 */
export interface INetworkCreationDescriptor {
  Name: string;
}
