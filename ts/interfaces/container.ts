import { DockerNetwork } from '../classes.network.js';

export interface IContainerCreationDescriptor {
  Hostname: string;
  Domainname: string;
  networks?: DockerNetwork[];
}
