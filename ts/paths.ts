import * as plugins from './plugins.js';

export const packageDir = plugins.path.resolve(
  plugins.smartpath.get.dirnameFromImportMetaUrl(import.meta.url),
  '../'
);

export const nogitDir = plugins.path.resolve(packageDir, '.nogit/');
plugins.smartfile.fs.ensureDir(nogitDir);
